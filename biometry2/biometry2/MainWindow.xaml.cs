﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Windows.Interop;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;
using System.ComponentModel;
using Color = System.Drawing.Color;
using System.Threading;

namespace biometry2
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Drawing.Color[,] pixels;
        private Bitmap bmp;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void loadFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki zdjęć (*.png;*.jpeg)|*.png;*.jpeg;*.jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                img.Source = new BitmapImage(new Uri(openFileDialog.FileName));
            }

            bmp = BitmapImage2Bitmap((BitmapImage)img.Source);
            pixels = new System.Drawing.Color[bmp.Width, bmp.Height];

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    pixels[i, j] = bmp.GetPixel(i, j);
                }
            }
        }

        private void paintImage()
        {
            bmp = BitmapImage2Bitmap((BitmapImage)img.Source);

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    bmp.SetPixel(i, j, pixels[i, j]);
                }
            }

            img.Source = Bitmap2BitmapImage(bmp);
        }

        private void toGrayscale(object sender, RoutedEventArgs e)
        {

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    int color = (pixels[i, j].R + pixels[i, j].G +
                        pixels[i, j].B) / 3;

                    pixels[i, j] = System.Drawing.Color.FromArgb(color, color, color);
                }
            }

            paintImage();
        }

        private System.Drawing.Color[,] performThresholding(int level)
        {
            System.Drawing.Color[,] result = new System.Drawing.Color[bmp.Width,bmp.Height];

            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {

                    int b = pixels[i, j].R;

                    if (b > level)
                    {
                        result[i, j] = System.Drawing.Color.FromArgb(255, 255, 255);
                    }

                    else
                    {
                        result[i, j] = System.Drawing.Color.FromArgb(0, 0, 0);
                    }
                }
            }

            return result;

        }

        private System.Drawing.Color[,] erosion(System.Drawing.Color[,] input)
        {
            System.Drawing.Color[,] result = new System.Drawing.Color[bmp.Width, bmp.Height];

            Color black = Color.FromArgb(0, 0, 0);

            for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    bool isBlack = false;
                    if (input[i, j] == black)
                    {
                        if (i == 0 || input[i - 1, j] == black)
                        {
                            if (i == input.GetLength(0) - 1 || input[i + 1, j] == black)
                            {
                                if (j == 0 || input[i, j - 1] == black)
                                {
                                    if (j == input.GetLength(1) - 1 || input[i, j + 1] == black)
                                    {
                                        isBlack = true;
                                        result[i,j] = black;
                                    }
                                }
                            }

                        }
                    }

                    if (!isBlack)
                    {
                        result[i, j] = Color.FromArgb(255, 255, 255);
                    }
                }
            }

            return result;
        }

        private System.Drawing.Color[,] dilation(System.Drawing.Color[,] input)
        {
            System.Drawing.Color[,] result = new System.Drawing.Color[bmp.Width, bmp.Height];

            for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    result[i, j] = Color.FromArgb(255, 255, 255);
                }
            }

            Color black = Color.FromArgb(0, 0, 0);

            for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    if (input[i, j] == black)
                    {
                        result[i, j] = black;

                        if (i != 0)
                        {
                            result[i - 1, j] = black;
                        }

                        if (i != input.GetLength(0) - 1)
                        {
                            result[i + 1, j] = black;
                        }

                        if (j != 0)
                        {
                            result[i, j - 1] = black;
                        }

                        if (j != input.GetLength(1) - 1)
                        {
                            result[i, j + 1] = black;
                        }
                    }
                }
            }
            return result;
        }

        private int findRadius(System.Drawing.Color[,] image, int xCenter, int yCenter, bool isPupil)
        {
            int radius = 0;
            System.Drawing.Color white = System.Drawing.Color.FromArgb(255, 255, 255);

            
                bool rightFound = false;
                bool leftFound = false;
                bool upFound = false;
                bool downFound = false;

                while (true)
                {
                    if (image[xCenter + radius, yCenter] == white)
                    {
                        rightFound = true;
                    }

                    if (image[xCenter - radius, yCenter] == white)
                    {
                        leftFound = true;
                    }

                    if (image[xCenter + radius, yCenter] == white)
                    {
                        downFound = true;
                    }

                    if (image[xCenter + radius, yCenter] == white)
                    {
                        upFound = true;
                    }


                    radius++;

                    if (rightFound && leftFound && upFound && downFound)
                    {
                        break;
                    }
                }



            return radius;
        }

        int xCenter = 0;
        int yCenter = 0;

        private void drawCircle(System.Drawing.Color[,] input, bool isPupil)
        {
            System.Drawing.Color[,] image = ( System.Drawing.Color[,]) input.Clone();

            int lastWhite = 0;
            System.Drawing.Color black = System.Drawing.Color.FromArgb(0, 0, 0);
            System.Drawing.Color white = System.Drawing.Color.FromArgb(255,255,255);

            


                for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = input.GetLength(1) - 1; j >= 0; j--)
                {
                    if (input[i, j] == white)
                    {
                        if (lastWhite < j) lastWhite = j;
                    }
                    else
                    {
                        if (lastWhite > j)
                        {
                            input[i, lastWhite] = black;
                            input[i, j] = white;
                            lastWhite--;
                        }
                    }
                }
            }


            bool found = false;

            for (int j = 0; j < input.GetLength(1); j++)
            {
                for (int i = 0; i < input.GetLength(0); i++)
                {
                    if (input[i, j] == black)
                    {
                        xCenter = i;
                        found = true;
                        break;
                    }

                    if (found) break;
                }
            }

            if (isPupil)
            {

                lastWhite = 0;
                input = (System.Drawing.Color[,]) image.Clone();

                for (int i = 0; i < input.GetLength(1); i++)
                {
                    for (int j = input.GetLength(0) - 1; j >= 0; j--)
                    {
                        if (pixels[j, i] == white)
                        {
                            if (lastWhite < j) lastWhite = j;
                        }
                        else
                        {
                            if (lastWhite > j)
                            {
                                pixels[lastWhite, i] = black;
                                pixels[j, i] = white;
                                lastWhite--;
                            }
                        }
                    }
                }

                found = false;

                for (int i = 0; i < input.GetLength(0); i++)
                {
                    for (int j = 0; j < input.GetLength(1); j++)
                    {
                        if (input[i, j] == black)
                        {
                            yCenter = j;
                            found = true;
                            break;
                        }

                        if (found) break;
                    }
                }

            }

            int radius = findRadius(image, xCenter, yCenter, isPupil);

            using (Graphics grf = Graphics.FromImage(bmp))
            {
                using (System.Drawing.Pen p = new System.Drawing.Pen(Color.Red))
                {
                    grf.DrawEllipse(p, new RectangleF(xCenter - radius, yCenter - radius, 2*radius, 2*radius));
                }
            }
        }


        private void findIris(object sender, RoutedEventArgs e)
        {

            System.Drawing.Color[,] iris = performThresholding(130);
            System.Drawing.Color[,] pupil = performThresholding(30);

            for (int i = 0; i < 2; i++)
            {
                iris = dilation(iris);
            }

            for (int i = 0; i < 7; i++)
            {
                pupil = dilation(pupil);
            }

            for (int i = 0; i < 7; i++)
            {
                
                pupil = erosion(pupil);
            }

            for (int i = 0; i < 8; i++)
            {
                iris = erosion(iris);
            }

            //pixels = iris;
            //paintImage();

            drawCircle(pupil, true);
            drawCircle(iris, false);

            img.Source = Bitmap2BitmapImage(bmp);

        }

        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }

    }
}
